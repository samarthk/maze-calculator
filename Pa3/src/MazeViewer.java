// Name: Samarth Kulkarni
// USC loginid: samarthk
// CS 455 PA3
// Spring 2017


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;


/**
 * MazeViewer class
 * 
 * Program to read in and display a maze and a path through the maze. At user
 * command displays a path through the maze if there is one.
 * 
 * How to call it from the command line:
 * 
 *      java MazeViewer mazeFile
 * 
 * where mazeFile is a text file of the maze. The format is the number of rows
 * and number of columns, followed by one line per row, followed by the start location, 
 * and ending with the exit location. Each maze location is
 * either a wall (1) or free (0). Here is an example of contents of a file for
 * a 3x4 maze, with start location as the top left, and exit location as the bottom right
 * (we count locations from 0, similar to Java arrays):
 * 
 * 3 4 
 * 0111
 * 0000
 * 1110
 * 0 0
 * 2 3
 * 
 */

public class MazeViewer {
   
   private static final char WALL_CHAR = '1';
   private static final char FREE_CHAR = '0';

   public static void main(String[] args)  {

	  String fileName = "";
      
	  try {

         if (args.length < 1) {
            System.out.println("ERROR: missing file name command line argument");
         }
         else {
            fileName = args[0];
            
            JFrame frame = readMazeFile(fileName);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            frame.setVisible(true);
         }

      }
      catch (FileNotFoundException exc) {
         System.out.println("File not found: " + fileName);
      }
      catch (IOException exc) {
         exc.printStackTrace();
      } catch (InValidDataException e) {
		System.out.println(e.getMessage());
	}
   }

   /**
    readMazeFile reads in maze from the file whose name is given and 
    returns a MazeFrame created from it.
   
   @param fileName
             the name of a file to read from (file format shown in class comments, above)
   @returns a MazeFrame containing the data from the file.
        
   @throws FileNotFoundException
              if there's no such file (subclass of IOException)
   @throws IOException
              (hook given in case you want to do more error-checking --
               that would also involve changing main to catch other exceptions)
   @throws InValidDataException
   				If there is problem with the data in the file.
   */
   private static MazeFrame readMazeFile(String fileName) throws IOException, InValidDataException{
	   File mazeFile = new File(fileName);
	      Scanner in = new Scanner(mazeFile);
	      boolean[][] mazeData;
	      MazeCoord entryLoc;
	      MazeCoord exitLoc;
	      try
	      {
	         if(!in.hasNextInt()){
	        	 throw new InValidDataException();
	         }
	         int row = in.nextInt();
	         
	         if(!in.hasNextInt()){
	        	 throw new InValidDataException();
	         }
	         int col = in.nextInt();
	         
	         mazeData = getMazeData(in, row, col);
	         entryLoc = getStartPt(in);
	         exitLoc = getEndPt(in);
	         
	      }
	      finally
	      {
	         in.close();
	      }
	   return new MazeFrame(mazeData, entryLoc, exitLoc);
   }

   /**
    * Reads exit location from the scanner object in.
    * @param in
    * @return Corresponding MazeCoord object if the exit location co-ordinates is valid.
    * @throws InValidDataException. If data is invalid.
    */
   private static MazeCoord getEndPt(Scanner in) throws InValidDataException {
	if(!in.hasNextInt()){
	   	 throw new InValidDataException("Invalid end point co-ordinates");
	    }
	    int x = in.nextInt();
	    
	    if(!in.hasNextInt()){
	   	 throw new InValidDataException("Invalid end point co-ordinates");
	    }
	    int y = in.nextInt();
	    
	    return new MazeCoord(x, y);
   }
   
   /**
    * Reads start location from the scanner object in.
    * @param in
    * @return Corresponding MazeCoord object if the start location co-ordinates is valid.
    * @throws InValidDataException. If data is invalid.
    */
	private static MazeCoord getStartPt(Scanner in) throws InValidDataException {
		if(!in.hasNext()){
	   	 throw new InValidDataException("No Starting point co-ordinates");
	    }
	    int x = in.nextInt();
	    
	    if(!in.hasNextInt()){
	   	 throw new InValidDataException("Invalid Start point co-ordinates");
	    }
	    int y = in.nextInt();
	    
	    return new MazeCoord(x, y);
	}

/**
 * Reads the maze data into a 2D array based on the input @param row and @param col from the Scanner @param in.
 
 * @return On successful retrieval. it returns complete 2D array. 
 
 * @throws InValidDataException if there is any mismatch with given input and data or type of the data.

 * PRE: valid row and col information.
 */
private static boolean[][] getMazeData(Scanner in, int row, int col) throws InValidDataException {
	boolean[][] mazeData = new boolean[row][col];
	
	for(int i = 0; i < row; i++){
		if(!in.hasNext()){
			throw new InValidDataException("Information related to cells at " + i +"th row is missing.");
		}
		else{
			String rowStr = in.next();
			for(int j = 0; j < col; j++){
				if(rowStr.length() != col){
					throw new InValidDataException("Not valid length of data at "+i+"th row.");
				}
				char cellData = rowStr.charAt(j);
				if(cellData == FREE_CHAR){
					mazeData[i][j] = false;
				}
				else if(cellData == WALL_CHAR){
					mazeData[i][j] = true;
				}
				else{
					throw new InValidDataException("Not valid cell data: "+cellData);
				}
			}
		}
	}
	return mazeData;
}
   
}
