// Name: Samarth Kulkarni
// USC loginid: samarthk
// CS 455 PA3
// Spring 2017

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ListIterator;

import javax.swing.JComponent;

/**
   MazeComponent class
   
   A component that displays the maze and path through it if one has been found.
*/
public class MazeComponent extends JComponent
{

   private static final int START_X = 10; // top left of corner of maze in frame
   private static final int START_Y = 10;
   private static final int BOX_WIDTH = 20;  // width and height of one maze "location"
   private static final int BOX_HEIGHT = 20;
   private static final int INSET = 2;// how much smaller on each side to make entry/exit inner box
   
   private Maze maze;
   
   /**
      Constructs the component.
      @param maze   the maze to display
   */
   public MazeComponent(Maze maze) 
   {  
      this.maze = maze;
   }

   
   /**
     Draws the current state of maze including the path through it if one has
     been found.
     @param g the graphics context
   */
   public void paintComponent(Graphics g)
   {
	   Graphics2D g2 = (Graphics2D) g;
	   
	   //Drawing outer boundary of maze.
	   Rectangle outline = new Rectangle(START_X-1, START_Y-1, (this.maze.numCols()*BOX_WIDTH)+2, (this.maze.numRows()*BOX_HEIGHT)+2);
	   g2.draw(outline);
	   
	   //Drawing maze based on maze data.
	   Rectangle loc;
	   for(int row = 0; row < this.maze.numRows(); row++){
		   for(int col = 0; col < this.maze.numCols(); col++){
			   loc = new Rectangle(START_X+((col)*BOX_WIDTH), START_Y+((row)*BOX_HEIGHT), BOX_WIDTH, BOX_HEIGHT);
			   if(this.maze.hasWallAt(new MazeCoord(row, col))){
				   g2.setColor(Color.BLACK);
				   g2.draw(loc);
				   g2.fill(loc);
			   }
			   else{
				   g2.setColor(Color.WHITE);
				   g2.draw(loc);
				   g2.fill(loc);
			   }
		   }
	   }
	   
	   //Marking entry position of maze.
	   Rectangle startLoc = new  Rectangle(START_X+((this.maze.getEntryLoc().getCol())*BOX_WIDTH)+INSET,
			   START_Y+((this.maze.getEntryLoc().getRow())*BOX_HEIGHT)+INSET, BOX_WIDTH-(2*INSET), BOX_HEIGHT-(2*INSET));
	   g2.setColor(Color.YELLOW);
	   g2.draw(startLoc);
	   g2.fill(startLoc);
	   
	   //Marking exit position of maze.
	   Rectangle exitLoc = new  Rectangle(START_X+((this.maze.getExitLoc().getCol())*BOX_WIDTH)+INSET,
			   START_Y+((this.maze.getExitLoc().getRow())*BOX_HEIGHT)+INSET, BOX_WIDTH-(2*INSET), BOX_HEIGHT-(2*INSET));
	   g2.setColor(Color.GREEN);
	   g2.draw(exitLoc);
	   g2.fill(exitLoc);
	   
	   
	   // If path was found. Drawing the path.
	   if(maze.getPath() != null && maze.getPath().size() > 0){
		   ListIterator<MazeCoord> iter = maze.getPath().listIterator();
		   MazeCoord prev = null;
		   Line2D line;
		   while(iter.hasNext()){
			   if(prev == null){
				   prev = iter.next();
			   }
			   else{
				   MazeCoord curr = iter.next();
				   
				   line = new Line2D.Double((START_X+((prev.getCol())*BOX_WIDTH)+(BOX_WIDTH/2)),(START_Y+((prev.getRow())*BOX_HEIGHT)+(BOX_HEIGHT/2)), 
						   (START_X+((curr.getCol())*BOX_WIDTH)+(BOX_WIDTH/2)),(START_Y+((curr.getRow())*BOX_HEIGHT)+(BOX_HEIGHT/2)));
				   g2.setColor(Color.BLUE);
				   g2.draw(line);
				   prev = curr;
			   }
		   }
	   }
   } 
}



