// Name: Samarth Kulkarni
// USC loginid: samarthk
// CS 455 PA3
// Spring 2017

import java.util.ArrayList;
import java.util.LinkedList;


/**
   Maze class

   Stores information about a maze and can find a path through the maze
   (if there is one).
   
   Assumptions about structure of the maze, as given in mazeData, startLoc, and endLoc
   (parameters to constructor), and the path:
     -- no outer walls given in mazeData -- search assumes there is a virtual 
        border around the maze (i.e., the maze path can't go outside of the maze
        boundaries)
     -- start location for a path is maze coordinate startLoc
     -- exit location is maze coordinate exitLoc
     -- mazeData input is a 2D array of booleans, where true means there is a wall
        at that location, and false means there isn't (see public FREE / WALL 
        constants below) 
     -- in mazeData the first index indicates the row. e.g., mazeData[row][col]
     -- only travel in 4 compass directions (no diagonal paths)
     -- can't travel through walls

 */

public class Maze {
   
   public static final boolean FREE = false;
   public static final boolean WALL = true;
   
   private int numRows;
   private int numCols;
   private boolean[][] mazeData;
   private MazeCoord entryLoc;
   private MazeCoord exitLoc;
   private LinkedList<MazeCoord> path = null;
   private ArrayList<MazeCoord> visited;
  

   /**
      Constructs a maze.
      @param mazeData the maze to search.  See general Maze comments above for what
      goes in this array.
      @param startLoc the location in maze to start the search (not necessarily on an edge)
      @param exitLoc the "exit" location of the maze (not necessarily on an edge)
      PRE: 0 <= startLoc.getRow() < mazeData.length and 0 <= startLoc.getCol() < mazeData[0].length
         and 0 <= endLoc.getRow() < mazeData.length and 0 <= endLoc.getCol() < mazeData[0].length

    */
   public Maze(boolean[][] mazeData, MazeCoord startLoc, MazeCoord exitLoc) {
	   this.mazeData = mazeData;
	   this.numRows = mazeData.length;
	   this.numCols = mazeData[0].length;
	   this.entryLoc = startLoc;
	   this.exitLoc = exitLoc;
   }


   /**
      Returns the number of rows in the maze
      @return number of rows
   */
   public int numRows() {
      return this.numRows;
   }

   
   /**
      Returns the number of columns in the maze
      @return number of columns
   */   
   public int numCols() {
      return this.numCols;
   } 
 
   
   /**
      Returns true iff there is a wall at this location
      @param loc the location in maze coordinates
      @return whether there is a wall here
      PRE: 0 <= loc.getRow() < numRows() and 0 <= loc.getCol() < numCols()
   */
   public boolean hasWallAt(MazeCoord loc) {
      return (this.mazeData[loc.getRow()][loc.getCol()]);
   }
   

   /**
      Returns the entry location of this maze.
    */
   public MazeCoord getEntryLoc() {
      return this.entryLoc;
   }
   
   
   /**
     Returns the exit location of this maze.
   */
   public MazeCoord getExitLoc() {
      return this.exitLoc;
   }

   
   /**
      Returns the path through the maze. First element is start location, and
      last element is exit location.  If there was not path, or if this is called
      before a call to search, returns empty list.

      @return the maze path
    */
   public LinkedList<MazeCoord> getPath() {

      return this.path;

   }


   /**
      Find a path from start location to the exit location (see Maze
      constructor parameters, startLoc and exitLoc) if there is one.
      Client can access the path found via getPath method.

      @return whether a path was found. Also, if exit location is on wall it will return false without even looking for path
      as it is not reachable.
    */
   public boolean search()  {  
      visited = new ArrayList<MazeCoord>();
      if(hasWallAt(exitLoc)){
    	  return false;
      }
      else{
    	  path = new LinkedList<MazeCoord>();
    	  return search(entryLoc.getRow(), entryLoc.getCol());
      }
   }


   /**
    * Helper method to find the path from start location to exit location.
    * 
    * @param row and @param col initially are co-ordinates of start location.
    * It will be called recursively until it finds path to exit location or run out of options.
    * 
    * @return true if path was found. Along with updating path co-ordinates to instance variable path. 
    */
	private boolean search(int row, int col) {
		if(isBoundaryWall(row, col)){
			return false;
		}
		if(hasWallAt(new MazeCoord(row, col))){
			visited.add(new MazeCoord(row, col));
			return false;
		}
		else if(isNodeVisited(new MazeCoord(row, col))){
			return false;
		}
		else if(exitLoc.equals(new MazeCoord(row, col))){
			visited.add(new MazeCoord(row, col));
			path.addFirst(new MazeCoord(row, col));
			return true;
		}
		visited.add(new MazeCoord(row, col));
		if(search(row+1, col)){
			path.addFirst(new MazeCoord(row, col));
			return true;
		}
		else if(search(row, col+1)){
			path.addFirst(new MazeCoord(row, col));
			return true;
		}
		else if(search(row-1, col)){
			path.addFirst(new MazeCoord(row, col));
			return true;
		}
		else if(search(row, col-1)){
			path.addFirst(new MazeCoord(row, col));
			return true;
		}
		return false;
	}

	/**
	 * It will check whether we have crossed the boundary of maze or not 
	 * @param row
	 * @param col
	 * @return true if boundary is crossed else false.
	 */
	private boolean isBoundaryWall(int row, int col) {
		if(row < 0 || row == numRows || col < 0 || col == numCols){
			   return true;
		   }
		return false;
	}

	/**
	 * Takes the MazeCoord object and determines whether that co-ordinate is already visited while find path recursively by checking in 
	 * instance variable visited. 
	 * @param mazeCoord
	 * @return true if already visited. else false.
	 */
	private boolean isNodeVisited(MazeCoord mazeCoord) {
		boolean isVisited = false;
		for(MazeCoord loc : this.visited){
			if(loc.equals(mazeCoord)){
				isVisited = true;
			}
		}
		return isVisited;
	}
}
