// Name: Samarth Kulkarni
// USC loginid: samarthk
// CS 455 PA3
// Spring 2017

/**
 * This will report in valid data.
 * @author Samarth Kulkarni
 *
 */
public class InValidDataException extends Exception {

	public InValidDataException() {
	}

	public InValidDataException(String msg) {
		super(msg);
	}

}
